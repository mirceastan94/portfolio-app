import { BrowserModule } from "@angular/platform-browser";
import { APP_INITIALIZER, NgModule } from "@angular/core";

import { AppRoutingModule } from "./core/routing/app-routing.module";
import { AngularMaterialModule } from "./core/material/angular-material.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FlexLayoutModule } from "@angular/flex-layout";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateModule, TranslateLoader, TranslateService } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { NgxTransService } from "./core/translate/translate.service";
import { AppComponent } from "./app.component";
import { AboutComponent } from "./sections/about/about-me.component";
import { SkillsComponent } from "./sections/skills/skills.component";
import { PortfolioComponent } from "./sections/portfolio/portfolio.component";
import { ExperienceComponent } from "./sections/experience/experience.component";
import { EducationComponent } from "./sections/education/education.component";
import { HobbiesComponent } from "./sections/hobbies/hobbies.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

export function appInitializerFactory(translate: TranslateService) {
  return () => {
    translate.setDefaultLang('en');
    return translate.use('en').toPromise();
  };
}

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    SkillsComponent,
    PortfolioComponent,
    ExperienceComponent,
    EducationComponent,
    HobbiesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    AngularMaterialModule,
    FlexLayoutModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [  {
    provide: APP_INITIALIZER,
    useFactory: appInitializerFactory,
    deps: [TranslateService],
    multi: true
  }],
  bootstrap: [AppComponent],
})
/**
 * This class generates the main app module
 */
export class AppModule {}
