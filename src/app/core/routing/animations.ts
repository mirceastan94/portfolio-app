import { transition, style, query, trigger, animate, group } from '@angular/animations';

export function routerAnimation() {
    return trigger('routerAnimation', [

        transition('-1 => *', [
            query(':enter', [
                style({
                    position: 'fixed',
                    width: '100%',
                    transform: 'translateX(-100%)',
                }),
                animate(
                    '450ms ease',
                    style({
                        opacity: 1,
                        transform: 'translateX(0%)',
                    }),
                ),
            ]),
        ]),

        transition(':decrement', [
            query(
                ':enter',
                style({
                    position: 'fixed',
                    width: '100%',
                    transform: 'translateX(-100%)',
                }),
            ),

            group([
                query(
                    ':leave',
                    animate(
                        '400ms ease',
                        style({
                            position: 'fixed',
                            width: '100%',
                            transform: 'translateX(100%)',
                        }),
                    ),
                ),
                query(
                    ':enter',
                    animate(
                        '700ms ease',
                        style({
                            opacity: 1,
                            transform: 'translateX(0%)',
                        }),
                    ),
                ),
            ]),
        ]),

        transition(':increment', [
            query(
                ':enter',
                style({
                    position: 'fixed',
                    width: '100%',
                    transform: 'translateX(100%)',
                }),
            ),

            group([
                query(
                    ':leave',
                    animate(
                        '600ms ease',
                        style({
                            position: 'fixed',
                            width: '100%',
                            transform: 'translateX(-100%)',
                        }),
                    ),
                ),
                query(
                    ':enter',
                    animate(
                        '700ms ease',
                        style({
                            opacity: 1,
                            transform: 'translateX(0%)',
                        }),
                    ),
                ),
            ]),
        ]),
    ]);
}
