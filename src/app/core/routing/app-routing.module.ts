import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutComponent } from 'src/app/sections/about/about-me.component';
import { PortfolioComponent } from 'src/app/sections/portfolio/portfolio.component';
import { EducationComponent } from 'src/app/sections/education/education.component';
import { ExperienceComponent } from 'src/app/sections/experience/experience.component';
import { SkillsComponent } from 'src/app/sections/skills/skills.component';
import { HobbiesComponent } from 'src/app/sections/hobbies/hobbies.component';

const routes: Routes = [
  { path: '', redirectTo: '/about', pathMatch: 'full' },
  { path: 'about', component: AboutComponent, data: { num: 1 } },
  { path: 'skills', component: SkillsComponent, data: { num: 2 } },
  { path: 'portfolio', component: PortfolioComponent, data: { num: 3 } },
  { path: 'experience', component: ExperienceComponent, data: { num: 4 } },
  { path: 'education', component: EducationComponent, data: { num: 5 } },
  { path: 'hobbies', component: HobbiesComponent, data: { num: 6 } },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
/**
 * This class exports the module for the use of routing's mechanism
 */
export class AppRoutingModule { }
