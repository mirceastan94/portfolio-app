import { Component, OnInit } from '@angular/core';
import { routerAnimation } from './core/routing/animations';
import { RouterOutlet } from '@angular/router';
import { NgxTransService } from './core/translate/translate.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    routerAnimation()
  ]
})

/**
 * This class generates the main, standard AppComponent
 */
export class AppComponent {

  constructor(private translateService: NgxTransService) {
  }

  changeLanguage(langValue: string) {
    this.translateService.applyLanguage(langValue);
  }

  /**
   * Retrieves the current activated route number
   * @param outlet
   */
  public getRouteAnimation(outlet: RouterOutlet) {
    return outlet.activatedRouteData.num === undefined ? -1 : outlet.activatedRouteData.num;
  }

}
