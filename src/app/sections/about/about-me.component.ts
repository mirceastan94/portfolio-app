import { Component, OnInit} from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'about-me',
  templateUrl: './about-me.component.html',
  styleUrls: ['./about-me.component.scss']
})

/**
 * This class generates the "About me" section component
 */
export class AboutComponent {}
