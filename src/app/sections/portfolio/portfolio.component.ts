import { Component, OnInit } from '@angular/core';
import { NgxTransService } from 'src/app/core/translate/translate.service';

@Component({
  selector: 'portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})

/**
 * This class generates the "Portfolio" section component
 */
export class PortfolioComponent implements OnInit {

  portfolioApplicationsInfo = [];

  constructor(private translateService: NgxTransService) { }

  ngOnInit() {
    this.portfolioApplicationsInfo = [
      {
        name: "weatherBot",
        imagePath: "../../../assets/img/portfolio/weatherbot/personal-search-history-and-choice-weather.jpg",
        text: this.translateService.getMessageTranslation("pictures-weatherbot-history")
      },
      {
        name: "weatherBot",
        imagePath: "../../../assets/img/portfolio/weatherbot/weather-and-forecast.jpg",
        text: this.translateService.getMessageTranslation("pictures-weatherbot-forecast")
      },
      {
        name: "musicManager",
        imagePath: "../../../assets/img/portfolio/manager/main-menu.jpg",
        text: this.translateService.getMessageTranslation("pictures-manager-main-menu")
      },
      {
        name: "musicManager",
        imagePath: "../../../assets/img/portfolio/manager/new-job-role-opening.jpg",
        text: this.translateService.getMessageTranslation("pictures-manager-new-job")
      },
      {
        name: "musicManager",
        imagePath: "../../../assets/img/portfolio/manager/jobs-search-list.jpg",
        text: this.translateService.getMessageTranslation("pictures-manager-job-search")
      },
      {
        name: "nextGenOnlineBazaar",
        imagePath: "../../../assets/img/portfolio/bazaar/home-page.JPG",
        text: this.translateService.getMessageTranslation("pictures-bazaar-home-page")
      },
      {
        name: "nextGenOnlineBazaar",
        imagePath: "../../../assets/img/portfolio/bazaar/products-grid.JPG",
        text: this.translateService.getMessageTranslation("pictures-bazaar-products-grid")
      },
      {
        name: "nextGenOnlineBazaar",
        imagePath: "../../../assets/img/portfolio/bazaar/order-details.JPG",
        text: this.translateService.getMessageTranslation("pictures-bazaar-order-details")
      },
      {
        name: "nextGenOnlineBazaar",
        imagePath: "../../../assets/img/portfolio/bazaar/admin-dashboard.JPG",
        text: this.translateService.getMessageTranslation("pictures-bazaar-admin-dashboard")
      },
    ]
  }

}
