import { Component, OnInit} from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})

/**
 * This class generates the "Education" section component
 */
export class EducationComponent {}
